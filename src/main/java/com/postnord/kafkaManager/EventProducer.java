package com.postnord.kafkaManager;

import com.postnord.api.dto.CreditCardDto;

import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Message;

import io.smallrye.mutiny.Uni;
import io.smallrye.reactive.messaging.MutinyEmitter;
import io.smallrye.reactive.messaging.annotations.Broadcast;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.Response;


@ApplicationScoped
public class EventProducer {

    @Inject
    @Channel("event-out")
    @Broadcast
    MutinyEmitter<CreditCardDto> emitter;

    public Uni<Response> produceEvent(final CreditCardDto creditCardDto) {
        return emitter.sendMessage(Message.of(creditCardDto))
                .map(entity -> Response
                        .ok(entity)
                        .build())
                .onFailure()
                .recoverWithItem(() -> Response
                        .status(500)
                        .build());
    }


}
