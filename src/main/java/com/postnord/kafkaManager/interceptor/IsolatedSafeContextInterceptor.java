package com.postnord.kafkaManager.interceptor;

import io.quarkus.vertx.core.runtime.context.VertxContextSafetyToggle;
import io.vertx.core.Vertx;
import jakarta.inject.Inject;
import jakarta.interceptor.AroundInvoke;
import jakarta.interceptor.Interceptor;
import jakarta.interceptor.InvocationContext;

@RunInSafeContext
@Interceptor
public class IsolatedSafeContextInterceptor {

    @Inject
    private Vertx vertx;

    @AroundInvoke
    public Object isolatedContext(InvocationContext ic) throws Exception {
        final io.vertx.core.Context newContext = vertx.getOrCreateContext();
        VertxContextSafetyToggle.setContextSafe(newContext, true);
        return ic.proceed();
    }
}
